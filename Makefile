# Uncomment lines below if you have problems with $PATH
SHELL := /usr/local/bin/zsh
#PATH := /usr/local/bin:$(PATH)

DEVICE=/dev/cu.usbmodem1423201

pngs = $(wildcard pngs/*.png)
bmps = $(wildcard bmps/*.bmp)

all:
	platformio -c vim run

format:
	clang-format -style=file -i src/*

upload:
	platformio -c vim run --target upload --upload-port ${DEVICE}

clean:
	platformio -c vim run --target clean

program:
	platformio -c vim run --target program

uploadfs:
	platformio -c vim run --target uploadfs

update:
	platformio -c vim update

monitor:
	platformio device monitor -b 115200 -p ${DEVICE}

update_firmware:
	esptool.py --port /dev/tty.usbmodem1423201 --before no_reset --baud 115200 write_flash 0 NINA_W102-1.7.4.bin
