const fetch = require("node-fetch");
const Jimp = require('jimp');

const success = {
  statusCode: 200,
  body: ""
};

const WIDTH = 64;
const HEIGHT = 32;
const header = 'data:image/bmp;base64,'

exports.handler = async function(event, context) {
  const { httpMethod, queryStringParameters } = event;
  if (httpMethod === 'GET') {
    const { id } = queryStringParameters;
    const url = `https://static-cdn.jtvnw.net/emoticons/v1/${id}/2.0`;
    try {
      const image = await Jimp.read(url)
      image.background(0);
      image.dither565()
      image.contain(WIDTH, HEIGHT, Jimp.HORIZONTAL_ALIGN_CENTER, Jimp.VERTICAL_ALIGN_MIDDLE)
      //image.resize(WIDTH, HEIGHT, Jimp.RESIZE_BEZIER)
      //image.cover(WIDTH, HEIGHT, Jimp.HORIZONTAL_ALIGN_CENTER, Jimp.VERTICAL_ALIGN_MIDDLE)
      const dataUri = await image.getBase64Async(Jimp.MIME_BMP);
      const body = dataUri.slice(header.length);
      return {
        headers: {
          "Content-Type": "image/bmp"
        },
        statusCode: 200,
        body,
        isBase64Encoded: true,
      };
    } catch (e) {
      console.log(e)
    }
  }
  return success;
}
