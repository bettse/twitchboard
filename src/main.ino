#define USE_AIRLIFT

#include "config.h"
#include "FakeFile.h"

#include "utility/wifi_drv.h"
#include <Adafruit_GFX.h>         // Core graphics library
#include <Adafruit_ImageReader.h> // Image-reading functions

#include <ArduinoJson.h> // https://github.com/bblanchon/ArduinoJson
#include <Adafruit_LIS3DH.h> // For accelerometer
#include <Adafruit_Protomatter.h> // For RGB matrix
#include <ArduinoHttpClient.h>
#include <SPI.h>
#include <WiFiNINA.h>
#include <Wire.h> // For I2C communication

// Adjust this number for the sensitivity of the 'click' force
// this strongly depend on the range! for 16G, try 5-10
// for 8G, try 10-20. for 4G try 20-40. for 2G try 40-80
#define CLICKTHRESHHOLD 80

// RGB MATRIX (PROTOMATTER) LIBRARY STUFF ----------------------------------

#define HEIGHT 32 // Matrix height (pixels) - SET TO 64 FOR 64x64 MATRIX!
#define WIDTH 64 // Matrix width (pixels)
#define BUTTON_UP 2
#define BUTTON_DOWN 3
#define SSL_PORT 443

uint8_t rgbPins[] = {7, 8, 9, 10, 11, 12};
uint8_t addrPins[] = {17, 18, 19, 20};
uint8_t clockPin = 14;
uint8_t latchPin = 15;
uint8_t oePin = 16;
int wifiStatus = WL_IDLE_STATUS;     // the Wifi radio's status
bool subscribed = false;
unsigned long baud = 115200;

Adafruit_Protomatter matrix(WIDTH, 4, 1, rgbPins, sizeof(addrPins), addrPins, clockPin, latchPin, oePin, false);
Adafruit_LIS3DH accel = Adafruit_LIS3DH();

WiFiSSLClient wsSocket;
WebSocketClient webSocket = WebSocketClient(wsSocket, "websocket.ericbetts.dev", SSL_PORT);

WiFiSSLClient httpSocket;
HttpClient http = HttpClient(httpSocket, "twitch.ericbetts.dev", SSL_PORT);

long previousMillis = 0;
long interval = 60 * 1000;

void printWifiData() {
  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
}

void connectWS() {
  webSocket.begin("/");
}

void dumpESP32Serial() {
  while (SerialESP32.available()) {
    Serial.write(SerialESP32.read());
  }
}

void show(const char *id) {
  Serial.printf("Show %s\n", id);
  dumpESP32Serial();
  char request[64] = {0};

  sprintf(request, "/.netlify/functions/twitch?id=%s", id);
  // disconnectWS();

  int res = http.get(request);
  if (res != 0) {
    Serial.printf("Error %d during get request\n", res);
  }
  dumpESP32Serial();

  Serial.print("Status code: ");
  // read the status code and body of the response
  int statusCode = http.responseStatusCode();
  Serial.println(statusCode);
  dumpESP32Serial();

  if (statusCode == 200) {
    String response = http.responseBody();
    dumpESP32Serial();
    ImageReturnCode stat;
    Adafruit_Image img;

    FakeFile f = FakeFile((uint8_t*) response.c_str(), response.length());
    Adafruit_ImageReader reader(f);
    stat = reader.coreBMP("", NULL, NULL, 0, 0, &img, false);
    Serial.print("Process BMP: ");
    reader.printStatus(stat, Serial);
    GFXcanvas16 *bmp = (GFXcanvas16*)(img.getCanvas());
    Serial.printf("width: %d, height: %d", bmp->width(), bmp->height());

    matrix.drawRGBBitmap(0, 0, bmp->getBuffer(), bmp->width(), bmp->height());
    matrix.show();
  }

  dumpESP32Serial();
  http.stop();
}

void onMessage(String payload) {
  size_t length = payload.length();
  if (length > 1 && length < 128 && payload[0] == '{' && payload[length - 1] == '}') {
    StaticJsonDocument<128> doc;
    DeserializationError error = deserializeJson(doc, payload);
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
      return;
    }

    Serial.printf("[WSc] got json:\n");
    serializeJsonPretty(doc, Serial);
    Serial.println("");

    if (doc["id"]) {
      const char* id = doc["id"];
      show(id);
    }
  } else {
    Serial.printf("Non JSON: %s\n", payload.c_str());
  }
}

void subscribe() {
  StaticJsonDocument<128> doc;
  doc["action"] = "subscribe";
  doc["key"] = "c2b0441c5e8272eb761ec952c92d6cf9a7d36acdc280239234f4e49bd1285518";
  String json;
  serializeJson(doc, json);
  webSocket.beginMessage(TYPE_TEXT);
  webSocket.print(json);
  webSocket.endMessage();
  subscribed = true;
}

void disconnectWS() {
  webSocket.stop();
  subscribed = false;
  dumpESP32Serial();
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON_UP, INPUT_PULLUP);
  pinMode(BUTTON_DOWN, INPUT_PULLUP);

  Serial.begin(baud);
  SerialESP32.begin(baud);
  //while (!Serial) delay(10);

  ProtomatterStatus status = matrix.begin();
  Serial.printf("Protomatter begin() status: %d\n", status);
  matrix.fillScreen(0);
  matrix.setRotation(2);
  matrix.setTextWrap(true);

  Serial.print("Connecting...");
  WiFi.setPins(SPIWIFI_SS, SPIWIFI_ACK, ESP32_RESETN, ESP32_GPIO0, &SPIWIFI);
  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    while (true);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < "1.0.0") {
    Serial.println("Please upgrade the firmware");
  }

  // attempt to connect to Wifi network:
  while (wifiStatus != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(WIFI_SSID);
    // Connect to WPA/WPA2 network:
    wifiStatus = WiFi.begin(WIFI_SSID, WIFI_PASS);

    // wait 10 seconds for connection:
    delay(10000);
  }

  // you're connected now, so print out the data:
  Serial.println("You're connected to the network");
  printWifiData();

  WiFiDrv::debug(1);
  dumpESP32Serial();

  matrix.println(WIFI_SSID);
  matrix.show();


  const char *id = "2";
  show(id);

  connectWS();

  if (!accel.begin(0x19)) {
    Serial.println("Couldn't find accelerometer");
  }
  accel.setRange(LIS3DH_RANGE_2_G); // 2, 4, 8 or 16 G!
  accel.setClick(1, CLICKTHRESHHOLD);
}

void loop() {
  dumpESP32Serial();
  if (wsSocket.connected()) {
    if (subscribed) {
      int messageSize = webSocket.parseMessage();
      if (messageSize > 0) {
        onMessage(webSocket.readString());
      }
    } else {
      subscribe();
    }
  } else {
    connectWS();
  }

/*
  if (digitalRead(BUTTON_UP) == LOW) {
    Serial.println(F("UP BUTTON."));
    matrix.fillScreen(0); // Clear screen
    matrix.show();
    while (digitalRead(BUTTON_UP) == LOW)
      ; // Wait for release
  }
  if (digitalRead(BUTTON_DOWN) == LOW) {
    Serial.println(F("DOWN BUTTON."));
    while (digitalRead(BUTTON_DOWN) == LOW)
      ; // Wait for release
  }
  tapCheck();
*/

  long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    //keepalive
    webSocket.beginMessage(TYPE_TEXT);
    webSocket.print("{\"keepalive\": true}");
    //NOTE: maybe webSocket.ping(); ?
    webSocket.endMessage();
  }
}

void tapCheck() {
  uint8_t click = accel.getClick();
  if (click == 0)
    return;
  if (!(click & 0x30))
    return;
  Serial.printf("Click detected (%x): ", click);
  if (click & 0x10) {
    Serial.print("single click\n");
  }
}
