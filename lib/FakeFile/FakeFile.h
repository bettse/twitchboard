#ifndef FakeFile_h
#define FakeFile_h

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

class FakeFile
{
  public:
    FakeFile(uint8_t *data, size_t len);
    bool close();
		int read();
		uint32_t position();
    bool seek(uint32_t pos);
    int read(void* buf, size_t nbyte);

  private:
    uint8_t *data;
    size_t max;
    uint32_t index;
};

#endif
