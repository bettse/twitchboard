#include "FakeFile.h"

FakeFile::FakeFile(uint8_t *data, size_t len): data(data), max(len), index(0) {
}

bool FakeFile::close() {
  return true;
}

int FakeFile::read() {
  uint8_t b;
  return read(&b, 1) == 1 ? b : -1;
}

uint32_t FakeFile::position() {
  return index;
}

bool FakeFile::seek(uint32_t pos) {
  if (pos > max) {
    return false;
  }
  index = pos;
  return true;
}

int FakeFile::read(void *buf, size_t nbyte){
  if (index + nbyte > max) {
    nbyte = max - index;
  }
  memcpy(buf, data + index, nbyte);
  index += nbyte;
  return nbyte;
}
